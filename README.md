Plasma is open source *homeserver* implementation of the [Matrix protocol](https://matrix.org/docs/spec/).

[Matrix](https://matrix.org/) is an open standard for interoperable, decentralised, real-time communication over IP.
It can be used to power Instant Messaging, VoIP/WebRTC signalling, Internet of Things communication -
or anywhere you need a standard HTTP API for publishing and subscribing to data whilst tracking the
conversation history.

**THIS PROJECT IS STILL UNDER HEAVY CONSTRUCTION**

# Features

*This sections describes features planned to be implemented, not features currently available.
See [CHANGELOG](CHANGELOG.md) for current features status.*

Plasma implements server side of current [Matrix API specifications](https://matrix.org/docs/spec/) :
 - [client-server API](https://matrix.org/docs/spec/client_server/r0.6.0) (0.6.0)
 - [server-server API](https://matrix.org/docs/spec/server_server/unstable.html) (unstable)
 - [application service API](https://matrix.org/docs/spec/application_service/unstable.html) (unstable)
 - [identity service API](https://matrix.org/docs/spec/identity_service/unstable.html) (unstable)
 - [push gateway API](https://matrix.org/docs/spec/push_gateway/unstable.html) (unstable)

## Supported API

Plasma currently implements the following API endpoints

### Client-server
- [X] `GET /_matrix/client/versions`
- [X] `GET /.well-known/matrix/client`

#### Login
- [X] `GET /_matrix/client/r0/login`
- [X] `POST /_matrix/client/r0/login`
- [X] `POST /_matrix/client/r0/logout`
- [X] `POST /_matrix/client/r0/logout/all`

#### Account registration and management
- [X] `POST /_matrix/client/r0/register`
- [ ] `POST /_matrix/client/r0/register/email/requestToken`
- [ ] `POST /_matrix/client/r0/register/msidsn/requestToken`
- [ ] `POST /_matrix/client/r0/account/password`
- [ ] `POST /_matrix/client/r0/account/password/email/requestToken`
- [ ] `POST /_matrix/client/r0/account/password/msidsn/requestToken`
- [ ] `POST /_matrix/client/r0/account/deactivate`
- [X] `POST /_matrix/client/r0/register/available`

#### Adding Account Administrative Contact Information

- [ ] `GET /_matrix/client/r0/account/3pid`
- [ ] `POST /_matrix/client/r0/account/3pid`
- [ ] `POST /_matrix/client/r0/account/3pid/delete`
- [ ] `POST /_matrix/client/r0/account/3pid/email/requestToken`
- [ ] `POST /_matrix/client/r0/account/3pid/msidsn/requestToken`

#### Current account information
- [X] `GET /_matrix/client/r0/account/whoami`

#### Capabilities negotiation
- [X] `GET /_matrix/client/r0/capabilities`

#### Filtering
- [X] `POST /_matrix/client/r0/user/{userId}/filter`
- [X] `GET /_matrix/client/r0/user/{userId}/filter/{filterId}`

#### Syncing
- [ ] `GET /_matrix/client/r0/sync`
- [ ] `GET /_matrix/client/r0/events`
- [ ] `GET /_matrix/client/r0/initialSync`
- [ ] `GET /_matrix/client/r0/events/{eventId}`

#### Getting events for a room
- [ ] `GET /_matrix/client/r0/rooms/{roomId}/event/{eventId}`
- [ ] `GET /_matrix/client/r0/rooms/{roomId}/state/{eventType}/{stateKey}`
- [ ] `GET /_matrix/client/r0/rooms/{roomId}/state/{eventType}`
- [ ] `GET /_matrix/client/r0/rooms/{roomId}/state`
- [ ] `GET /_matrix/client/r0/rooms/{roomId}/members`
- [ ] `GET /_matrix/client/r0/rooms/{roomId}/joined_members`
- [ ] `GET /_matrix/client/r0/rooms/{roomId}/messages`
- [ ] `GET /_matrix/client/r0/rooms/{roomId}/initialSync`

#### Sending events to a room

- [ ] `PUT /_matrix/client/r0/rooms/{roomId}/state/{eventType}/{stateKey}`
- [ ] `PUT /_matrix/client/r0/rooms/{roomId}/state/{eventType}`
- [ ] `PUT /_matrix/client/r0/rooms/{roomId}/send/{eventType}/{txnId}`

#### Redaction
- [ ] `PUT /_matrix/client/r0/rooms/{roomId}/redact/{eventId}/{txnId}`

#### Room creation
- [X] `POST /_matrix/client/r0/createRoom`

#### Room aliases
- [ ] `PUT /_matrix/client/r0/directory/room/{roomAlias}`
- [ ] `GET /_matrix/client/r0/directory/room/{roomAlias}`
- [ ] `DELETE /_matrix/client/r0/directory/room/{roomAlias}`

#### Room membership
- [ ] `GET /_matrix/client/r0/joined_rooms`

##### Joining rooms
- [ ] `POST /_matrix/client/r0/rooms/{roomId}/invite`
- [ ] `POST /_matrix/client/r0/rooms/{roomId}/join`
- [ ] `POST /_matrix/client/r0/join/{roomIdOrAlias}`

##### Leaving rooms
- [ ] `POST /_matrix/client/r0/rooms/{roomId}/leave`
- [ ] `POST /_matrix/client/r0/rooms/{roomId}/forget`
- [ ] `POST /_matrix/client/r0/rooms/{roomId}/kick`

##### Banning users in a room
- [ ] `POST /_matrix/client/r0/rooms/{roomId}/ban`
- [ ] `POST /_matrix/client/r0/rooms/{roomId}/unban`

#### Listing rooms
- [ ] `GET /_matrix/client/r0/directory/list/room/{roomId}`
- [ ] `PUT /_matrix/client/r0/directory/list/room/{roomId}`
- [ ] `GET /_matrix/client/r0/publicRooms`
- [ ] `POST /_matrix/client/r0/publicRooms`

#### User Directory
- [ ] `POST /_matrix/client/r0/user_directory/search`

#### Profiles
- [ ] `PUT /_matrix/client/r0/profile/{userId}/displayname`
- [ ] `GET /_matrix/client/r0/profile/{userId}/displayname`
- [ ] `PUT /_matrix/client/r0/profile/{userId}/avatar_url`
- [ ] `GET /_matrix/client/r0/profile/{userId}/avatar_url`
- [ ] `GET /_matrix/client/r0/profile/{userId}`

#### Presence
- [X] `PUT /_matrix/client/r0/presence/{userId}/status`
- [X] `GET /_matrix/client/r0/presence/{userId}/status`

### Device Mangement
- [X] `GET /_matrix/client/r0/devices`
- [X] `GET /_matrix/client/r0/devices/{deviceId}`
- [X] `PUT /_matrix/client/r0/devices/{deviceId}`
- [ ] `DELETE /_matrix/client/r0/devices/{deviceId}`
- [ ] `POST /_matrix/client/r0/delete_devices`

# Contributing

Contributions are welcome. Fill free to clone this repo, open issues, push PR, ...
You can also join the community on the project matrix room: [#plasma:beerfactory.org](https://matrix.to/#/!gaSksXUwCVFzPPqDNJ:beerfactory.org?via=beerfactory.org&via=matrix.org&via=poddery.com&via=raim.ist).

Plasma is written in [Elixir language](https://elixir-lang.org/) and relies on the Erlang VM which provides
a scalable platform.

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
