defmodule PlasmaWeb.Plugs.RequireAccessTokenTest do
  use PlasmaWeb.ConnCase
  import PlasmaRepo.Factory
  import Phoenix.Controller, only: [json: 2]

  alias PlasmaWeb.Plugs.RequireAccessToken
  alias PlasmaRepo.Users.{AccessTokens, AccessToken}

  @opts RequireAccessToken.init([])

  describe "RequireAccessToken" do
    test "test Conn pass if token is assigned" do
      # Create user
      device = insert(:device)
      assert {:ok, token} = AccessTokens.encode_token(device, device.user)

      conn =
        build_conn()
        |> assign(:access_token, token)
        |> RequireAccessToken.call(@opts)

      assert conn.halted == false
      assert Map.get(conn.assigns, :user_id) == to_string(device.user.id)
      assert Map.get(conn.assigns, :device_id) == to_string(device.id)
    end

    test "test Conn halted if token is assigned but unkown" do
      {:ok, token, _claims} = PlasmaWeb.Guardian.encode_and_sign(%AccessToken{id: "c9ddd80c-9c84-4ac6-874f-4ca350619980"})

      conn =
        build_conn()
        |> assign(:access_token, token)
        |> RequireAccessToken.call(@opts)

      assert conn.halted == true
      assert conn.status == 401

      assert conn.resp_body ==
               json(build_conn(), %{
                 errcode: "M_UNKNOWN_TOKEN",
                 error: "Unknown access token"
               }).resp_body
    end

    test "test Conn halted if not token is assigned" do
      conn = build_conn() |> RequireAccessToken.call(@opts)
      assert conn.halted == true
      assert conn.status == 401

      assert conn.resp_body ==
               json(build_conn(), %{
                 errcode: "M_MISSING_TOKEN",
                 error: "Access token missing"
               }).resp_body
    end
  end
end
