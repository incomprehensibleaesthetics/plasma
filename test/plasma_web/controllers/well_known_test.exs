defmodule PlasmaWeb.Controllers.WellKnownTest do
  use PlasmaWeb.ConnCase

  @homeserver_base_url Application.get_env(:plasma, :homeserver_base_url)
  @identity_server Application.get_env(:plasma, :identity_server)

  test "get/2 returns current configuration", %{conn: conn} do
    response =
      conn
      |> get(Routes.well_known_path(conn, :get))
      |> json_response(200)

    assert response == %{
             "m.homeserver" => %{"base_url" => @homeserver_base_url},
             "m.identity_server" => %{"base_url" => @identity_server}
           }
  end
end
