defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.UserControllerTest do
  use PlasmaWeb.ConnCase

  @filter_request %{
    "room" => %{
      "state" => %{
        "types" => ["m.room.*"],
        "not_rooms" => ["!726s6s6q:example.com"]
      },
      "timeline" => %{
        "limit" => 10,
        "types" => ["m.room.message"],
        "not_rooms" => ["!726s6s6q:example.com"],
        "not_senders" => ["@spam:example.com"]
      },
      "ephemeral" => %{
        "types" => ["m.receipt", "m.typing"],
        "not_rooms" => ["!726s6s6q:example.com"],
        "not_senders" => ["@spam:example.com"]
      }
    },
    "presence" => %{
      "types" => ["m.presence"],
      "not_senders" => ["@alice:example.com"]
    },
    "event_format" => "client",
    "event_fields" => ["type", "content", "sender"]
  }

  setup %{conn: conn} do
    PlasmaWeb.Plugs.AuthSessionCache.flush()
    PlasmaRepo.Users.AccessTokenCache.flush()
    PlasmaRepo.Events.EventTypeCache.flush()

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), %{
        "auth" => %{"session" => get_auth_session(conn), "type" => "m.login.dummy"}
      })
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    [access_token: access_token, user_id: user_id]
  end

  test "create filter succeed with nominal case", %{conn: conn, access_token: access_token, user_id: user_id} do

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.user_path(conn, :post_filter, user_id), @filter_request)
      |> json_response(200)

    assert %{"filter_id" => filter_id} = response
  end

  test "get filter succeeds with existing filter", %{conn: conn, access_token: access_token, user_id: user_id} do
    %{"filter_id" => filter_id} = conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.user_path(conn, :post_filter, user_id), @filter_request)
      |> json_response(200)

    conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> get(Routes.user_path(conn, :get_filter, user_id, filter_id))
      |> json_response(200)
  end

  test "get filter fails with unknown filter", %{conn: conn, access_token: access_token, user_id: user_id} do
    response = conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> get(Routes.user_path(conn, :get_filter, user_id, "UKNOWN"))
    assert response.status == 404

  end

end