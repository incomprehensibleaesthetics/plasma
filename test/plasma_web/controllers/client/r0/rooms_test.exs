defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.RoomsTest do
  use PlasmaWeb.ConnCase

  setup %{conn: conn} do
    PlasmaWeb.Plugs.AuthSessionCache.flush()
    PlasmaRepo.Users.AccessTokenCache.flush()
    PlasmaRepo.Events.EventTypeCache.flush()

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), %{
        "auth" => %{"session" => get_auth_session(conn), "type" => "m.login.dummy"}
      })
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    [access_token: access_token]
  end

  test "create_room with unsupported room version fails", %{conn: conn, access_token: access_token} do
    request = %{"room_version" => "-1"}

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.room_path(conn, :create_room), request)
      |> json_response(400)

    assert %{"errcode" => "M_UNSUPPORTED_ROOM_VERSION"} = response
  end

  test "create_room with defaults succeed", %{conn: conn, access_token: access_token} do
    request = %{}

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.room_path(conn, :create_room), request)
      |> json_response(200)

    assert %{"room_id" => room_id} = response
  end

  test "create_room fails with invalid visibility", %{conn: conn, access_token: access_token} do
    request = %{"visibility" => "invisible"}

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.room_path(conn, :create_room), request)
      |> json_response(400)

    assert %{"errcode" => errcode} = response
    assert errcode == "M_BAD_JSON"
  end
end
