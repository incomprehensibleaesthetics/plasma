defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.PresenceControllerTest do
  use PlasmaWeb.ConnCase

  setup %{conn: conn} do
    PlasmaWeb.Plugs.AuthSessionCache.flush()
    PlasmaRepo.Users.AccessTokenCache.flush()
    PlasmaRepo.Events.EventTypeCache.flush()

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), %{
        "auth" => %{"session" => get_auth_session(conn), "type" => "m.login.dummy"}
      })
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    [access_token: access_token, user_id: user_id]
  end

  describe "PUT /_matrix/client/r0/presence/:userId/status" do
    test "returns M_BAD_JSON if missing presence key", %{
      conn: conn,
      access_token: access_token,
      user_id: user_id
    } do
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> put(Routes.presence_path(conn, :update, user_id), %{})
        |> json_response(400)

      assert %{"errcode" => errcode} = response
      assert errcode == "M_BAD_JSON"
    end

    test "returns M_FORBIDDEN if auth'd user does not match :user_id", %{
      conn: conn,
      access_token: access_token,
      user_id: _user_id
    } do
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> put(Routes.presence_path(conn, :update, "@wrong:localhost"), %{
          presence: "offline",
          status_msg: "HACKED"
        })
        |> json_response(403)

      assert %{"errcode" => errcode} = response
      assert errcode == "M_FORBIDDEN"
    end

    test "returns success when auth'd user matches :user_id and updating just presence", %{
      conn: conn,
      access_token: access_token,
      user_id: user_id
    } do
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> put(Routes.presence_path(conn, :update, user_id), %{presence: "unavailable"})
        |> json_response(200)

      assert %{} = response
    end

    test "returns success when auth'd user matches :user_id and updating presence and status_msg",
         %{
           conn: conn,
           access_token: access_token,
           user_id: user_id
         } do
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> put(Routes.presence_path(conn, :update, user_id), %{
          presence: "unavailable",
          status_msg: "in a meeting"
        })
        |> json_response(200)

      assert %{} = response
    end
  end

  describe "GET /_matrix/client/r0/presence/:userId/status" do
    test "returns the user's presence status", %{
      conn: conn,
      access_token: access_token,
      user_id: user_id
    } do
      # Given presence has already been added with a PUT request
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> put(Routes.presence_path(conn, :update, user_id), %{presence: "unavailable"})
      |> json_response(200)

      # When a GET request is sent
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> get(Routes.presence_path(conn, :show, user_id))
        |> json_response(200)

      # Then returns presence
      assert %{"presence" => "unavailable", "last_active_ago" => 0, "status_msg" => nil} =
               response
    end
  end
end
