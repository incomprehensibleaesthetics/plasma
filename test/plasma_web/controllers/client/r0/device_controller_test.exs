defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.DeviceControllerTest do
  use PlasmaWeb.ConnCase

  setup %{conn: conn} do
    PlasmaWeb.Plugs.AuthSessionCache.flush()
    PlasmaRepo.Users.AccessTokenCache.flush()
    PlasmaRepo.Events.EventTypeCache.flush()

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), %{
        "auth" => %{"session" => get_auth_session(conn), "type" => "m.login.dummy"}
      })
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    user = PlasmaRepo.Users.get_by_mx_user_id(user_id)

    [access_token: access_token, user_id: user_id, user: user]
  end

  describe "GET /_matrix/client/r0/devices" do
    test "returns information for all devices", %{
      conn: conn,
      user: user,
      access_token: access_token
    } do
      # given
      device_id = "test_device"
      display_name = "Test Device"

      assert {:ok, device} =
               PlasmaRepo.Users.add_device(%{
                 "user_id" => user.id,
                 "mx_device_id" => device_id,
                 "display_name" => display_name
               })

      # when
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> get(Routes.device_path(conn, :index))
        |> json_response(200)

      # then
      assert %{"devices" => devices} = response
      assert length(devices) == 2

      assert %{
               "device_id" => device_id,
               "display_name" => display_name,
               "last_seen_ip" => last_seen_ip,
               "last_seen_ts" => last_seen_ts
             } = List.first(devices)
    end
  end

  describe "GET /_matrix/client/r0/devices/{deviceId}" do
    test "returns information for specified device", %{
      conn: conn,
      user: user,
      access_token: access_token
    } do
      # given
      device_id = "test_device"
      display_name = "Test Device"

      assert {:ok, device} =
               PlasmaRepo.Users.add_device(%{
                 "user_id" => user.id,
                 "mx_device_id" => device_id,
                 "display_name" => display_name
               })

      # when
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> get(Routes.device_path(conn, :show, device_id))
        |> json_response(200)

      # then
      assert %{
               "device_id" => device_id,
               "display_name" => display_name,
               "last_seen_ip" => last_seen_ip,
               "last_seen_ts" => last_seen_ts
             } = response

      assert device_id == "test_device"
      assert display_name == "Test Device"
    end

    test "returns M_NOT_FOUND when device does not exist", %{
      conn: conn,
      user: _user,
      access_token: access_token
    } do
      # given
      device_id = "test_device"

      # when
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> get(Routes.device_path(conn, :show, device_id))
        |> json_response(404)

      # then
      assert %{"errcode" => errcode} = response
      assert errcode == "M_NOT_FOUND"
    end
  end

  describe "PUT /_matrix/client/r0/devices/{deviceId}" do
    test "updates the display name of the specified device", %{
      conn: conn,
      user: user,
      access_token: access_token
    } do
      # given
      device_id = "test_device"
      display_name = "Test Device"
      new_display_name = "New Display Name"

      assert {:ok, device} =
               PlasmaRepo.Users.add_device(%{
                 "user_id" => user.id,
                 "mx_device_id" => device_id,
                 "display_name" => display_name
               })

      # when
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> put(Routes.device_path(conn, :update, device_id), %{"display_name" => new_display_name})
      |> json_response(200)

      # then
      device = PlasmaRepo.Users.Devices.get_by_mx_device_id(device_id)
      assert device.display_name == new_display_name
    end

    test "returns M_NOT_FOUND when device does not exist", %{
      conn: conn,
      user: _user,
      access_token: access_token
    } do
      # given
      device_id = "test_device"

      # when
      response =
        conn
        |> put_req_header("content-type", "application/json")
        |> put_req_header("user-agent", "TEST")
        |> put_req_header("authorization", "Bearer " <> access_token)
        |> get(Routes.device_path(conn, :show, device_id))
        |> json_response(404)

      # then
      assert %{"errcode" => errcode} = response
      assert errcode == "M_NOT_FOUND"
    end
  end
end
