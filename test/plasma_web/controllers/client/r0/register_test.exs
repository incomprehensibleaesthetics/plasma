defmodule PlasmaWeb.Controllers.MatrixApi.Client.RegisterTest do
  use PlasmaWeb.ConnCase
  alias Plasma.Utils.Randomizer

  def clear_cache(_context) do
    PlasmaWeb.Plugs.AuthSessionCache.flush()
    PlasmaRepo.Users.AccessTokenCache.flush()
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  test "register/2 with all default succeed", %{conn: conn} do
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      }
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response
  end

  test "register/2 with username and password", %{conn: conn} do
    username = Randomizer.randomizer(8,:downcase)
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      },
      "username" => username,
      "password" => "password"
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    assert user_id == "@#{username}:localhost"
  end

  test "register/2 with username, password and bind options", %{conn: conn} do
    username = Randomizer.randomizer(8,:downcase)
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
        },
    "username" => username,
    "password" => "password",
    "bind_email" => true,
    "bind_msisdn" => true,
    "x_show_msisdn" => true
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    assert user_id == "@#{username}:localhost"
  end

  test "register/2 fails when username already exits", %{conn: conn} do
    username = Randomizer.randomizer(8,:downcase)
    # Create first user
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      },
      "username" => username,
      "password" => "password"
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    assert user_id == "@#{username}:localhost"

    # Create second user with same localpart
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      },
      "username" => username,
      "password" => "password"
    }

    response =
      build_conn()
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(400)

    assert %{"errcode" => errcode, "error" => message} = response
    assert errcode = "M_USER_IN_USE"
  end

  test "register/2 fails when username is invalid", %{conn: conn} do
    # Create first user
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      },
      "username" => "@" <> Randomizer.randomizer(8, :downcase),
      "password" => "password"
    }

    response =
      build_conn()
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(400)

    assert %{"errcode" => errcode, "error" => message} = response
    assert errcode == "M_INVALID_USERNAME"
  end

  test "available returns true for a valid free username", %{conn: conn} do
    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> get(Routes.register_path(conn, :available), username: "my_cool_localpart")
      |> json_response(200)

    assert %{"available" => true} = response
  end

  test "available returns M_INVALID_USERNAME for an invalid username", %{conn: conn} do
    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> get(Routes.register_path(conn, :available), username: "my@cool@localpart")
      |> json_response(400)

    assert %{"errcode" => errcode, "error" => message} = response
    assert errcode == "M_INVALID_USERNAME"
  end

  test "available returns M_USER_IN_USE with existing user", %{conn: conn} do
    # Register a new user
    request = %{
      "auth" => %{
        "session" => get_auth_session(conn),
        "type" => "m.login.dummy"
      }
    }

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => _access_token, "device_id" => _device_id, "user_id" => user_id} = response

    {:ok, identifier} = PlasmaRepo.Channels.Identifier.parse(user_id)
    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("user-agent", "TEST")
      |> get(Routes.register_path(conn, :available), username: identifier.localpart)
      |> json_response(400)

    assert %{"errcode" => errcode, "error" => message} = response
    assert errcode == "M_USER_IN_USE"
  end

end
