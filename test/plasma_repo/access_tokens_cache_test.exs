defmodule Plasma.Users.AccessTokensTest do
  use PlasmaRepo.DataCase
  import PlasmaRepo.Factory
  alias PlasmaRepo.Users.{AccessTokens, AccessToken}

  def clear_cache(_context) do
    PlasmaRepo.Users.AccessTokenCache.flush()
    :ok
  end

  setup :clear_cache

  describe "AccessTokens" do
    test "Access access token on existing user works" do
      device = insert(:device)
      assert {:ok, token} = AccessTokens.encode_token(device, device.user)
    end

    test "Decode access token contain original device and user id" do
      device = insert(:device)
      assert {:ok, token} = AccessTokens.encode_token(device, device.user)
      assert {:ok, access_token_id, _claims} = PlasmaWeb.Guardian.resource_from_token(token)
      access_token = PlasmaRepo.Users.AccessTokenCache.get(access_token_id)
      assert %AccessToken{} = access_token
      assert device.id == access_token.device.id
      assert device.user.id == access_token.user.id
    end
  end
end
