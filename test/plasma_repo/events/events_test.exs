defmodule PlasmaRepo.Events.EventsTest do
  use PlasmaRepo.DataCase
  import PlasmaRepo.Factory

  def clear_cache(_context) do
    PlasmaRepo.Events.EventTypeCache.flush()
    :ok
  end

  setup :clear_cache

  describe "Events" do
    test "create_event/2 succeeds" do
      room = insert(:room)
      event_type = insert(:event_type)
      sender = insert(:user)

      create_event_res =
        PlasmaRepo.Events.create_event(%{
          mx_event_id: Plasma.Utils.Randomizer.unique_id(),
          event_type_id: event_type.id,
          room_id: room.id,
          sender_id: sender.id
        })

      assert {:ok, _event} = create_event_res
    end

    test "create_event/2 succeeds without Matrix event_id" do
      room = insert(:room)
      event_type = insert(:event_type)
      sender = insert(:user)

      create_event_res =
        PlasmaRepo.Events.create_event(%{
          event_type_id: event_type.id,
          room_id: room.id,
          sender_id: sender.id
        })

      assert {:ok, _event} = create_event_res
    end

    test "create_event/2 fails without sender" do
      room = insert(:room)
      event_type = insert(:event_type)

      event =
        PlasmaRepo.Events.create_event(%{
          mx_event_id: Plasma.Utils.Randomizer.unique_id(),
          event_type_id: event_type.id,
          room_id: room.id
        })

      assert {:error, :event, _, _} = event
    end

    test "create_event/2 fails with unknown sender" do
      room = insert(:room)
      event_type = insert(:event_type)

      event =
        PlasmaRepo.Events.create_event(%{
          mx_event_id: Plasma.Utils.Randomizer.unique_id(),
          event_type_id: event_type.id,
          room_id: room.id,
          sender_id: UUID.uuid4()
        })

      assert {:error, :event, _, _} = event
    end

    test "create_event/2 fails without room" do
      event_type = insert(:event_type)
      sender = insert(:user)

      event =
        PlasmaRepo.Events.create_event(%{
          mx_event_id: Plasma.Utils.Randomizer.unique_id(),
          event_type_id: event_type.id,
          sender_id: sender.id
        })

      assert {:error, :event, _, _} = event
    end
  end
end
