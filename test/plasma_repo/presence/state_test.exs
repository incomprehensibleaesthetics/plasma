defmodule PlasmaRepo.Presence.StateTest do
  use ExUnit.Case, async: true
  alias PlasmaRepo.Presence.State

  setup do
    {:ok, presence} = State.start_link([])
    %{presence: presence}
  end

  test "gets current presence state", %{presence: presence} do
    assert State.get(presence) == %State{
             last_active_ago: 0,
             presence: "offline",
             status_msg: nil
           }
  end

  test "updates current presence state", %{presence: presence} do
    State.put(presence, %{last_active_ago: 500})

    assert State.get(presence) == %State{
             last_active_ago: 500,
             status_msg: nil,
             presence: "offline"
           }
  end
end
