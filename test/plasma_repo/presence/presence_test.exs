defmodule PlasmaRepo.Presence.Test do
  use ExUnit.Case, async: true
  alias PlasmaRepo.Presence
  alias PlasmaRepo.Presence.State

  test "get will create if agent does not exist" do
    user_id = "@user:localhost"
    {:ok, presence_agent} = Presence.get(user_id)
    presence = State.get(presence_agent)
    assert presence == %State{presence: "offline", last_active_ago: 0}
  end
end
