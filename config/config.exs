# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :plasma, PlasmaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "82piCs325d914i67U4IWFnQ3iJloRjVnkyD4tWKjft4PGgz0LPByD9l6j4QxxRDZ",
  render_errors: [view: PlasmaWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Plasma.PubSub, adapter: Phoenix.PubSub.PG2]

config :plasma, PlasmaWeb.Guardian,
  issuer: "plasma",
  # "Secret key. You can use `mix guardian.gen.secret` to get one"
  secret_key: "ms2LsBHcbO04Mjnr4SwkE7WeD6kcrwmyzF/xIRdUlImQTOlgffIguUuWor+KmBCm"

config :plasma,
  domain: "localhost",
  homeserver_base_url: "https://localhost/",
  identity_server: "https://localhost/"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Use Jason for JSON parsing in Postgrex
config :postgrex, :json_library, Jason

config :plasma, PlasmaWeb.Users.AccessTokenCache,
  gc_interval: 86_400 # 24 hrs

config :plasma, PlasmaWeb.Plugs.AuthSessionCache.Primary,
  gc_interval: 86_400 # 24 hrs

# Distributed Cache
config :plasma, PlasmaWeb.Plugs.AuthSessionCache,
  local: PlasmaWeb.Plugs.AuthSessionCache.Primary,
  node_selector: Nebulex.Adapters.Dist

config :plasma, :matrix,
  auth_flows: %{
    flows: [
      %{
        stages: ["m.login.dummy"]
      }
    ],
    params: %{},
    # 10 minutes
    session_ttl: 600
  },
  default_power_levels: %{
    ban: 50,
    events: %{},
    events_default: 0,
    invite: 50,
    kick: 50,
    redact: 50,
    state_default: 50,
    users: %{},
    users_default: 0,
    notifications: %{}
  },
  default_content_rules: [
    %{
      "rule_id" => ".m.rule.contains_user_name",
      "default" => true,
      "enabled" => true,
      "pattern" => "[the local part of the user's Matrix ID]",
      "actions" => [
        "notify",
        %{ "set_tweak" => "sound", "value" => "default"}
      ]
    }
  ],
  default_override_rules: [
    %{
      "rule_id" => ".m.rule.master",
      "default" => true,
      "enabled" => false,
      "conditions" => [],
      "actions" => [
        "dont_notify"
      ]
    },
    %{
      "rule_id" => ".m.rule.suppress_notices",
      "default" => true,
      "enabled" => true,
      "conditions" => [
        %{
          "kind" => "event_match",
          "key" => "content.msgtype",
          "pattern" => "m.notice"
        }
      ],
      "actions" => ["dont_notify"]
    },
    %{
      "rule_id" => ".m.rule.invite_for_me",
      "default" => true,
      "enabled" => true,
      "conditions" => [
        %{
          "key" => "type",
          "kind" => "event_match",
          "pattern" => "m.room.member"
        },
        %{
          "key" => "content.membership",
          "kind" => "event_match",
          "pattern" => "invite"
        },
        %{
          "key" => "state_key",
          "kind" => "event_match",
          "pattern" => "[the user's Matrix ID]"
        }
      ],
      "actions" => [
        "notify",
        %{
          "set_tweak" => "sound",
          "value" => "default"
        },
        %{
          "set_tweak" => "highlight",
          "value" => false
        }
      ]
    },
    %{
      "rule_id" => ".m.rule.member_event",
      "default" => true,
      "enabled" => true,
      "conditions" => [
        %{
          "key" => "type",
          "kind" => "event_match",
          "pattern" => "m.room.member"
        }
      ],
      "actions" => ["dont_notify"]
    },
    %{
      "rule_id" => ".m.rule.contains_display_name",
      "default" => true,
      "enabled" => true,
      "conditions" => [
        %{"kind" => "contains_display_name" }
      ],
      "actions" => [
        "notify",
        %{
          "set_tweak" => "sound",
          "value" => "default"
        },
        %{
          "set_tweak" => "highlight"
        }
      ]
    },
    %{
      "rule_id" => ".m.rule.roomnotif",
      "default" => true,
      "enabled" => true,
      "conditions" => [
        %{
          "kind" => "event_match",
          "key" => "content.body",
          "pattern" => "@room"
        },
        %{
          "kind" => "sender_notification_permission",
          "key" => "room"
        }
      ],
      "actions" => [
        "notify",
        %{
          "set_tweak" => "highlight",
          "value" => true
        }
      ]
    }
  ]

config :plasma, PlasmaRepo.Repo,
       migration_primary_key: [name: :id, type: :binary_id],
       migration_timestamps: [type: :utc_datetime]

config :plasma,
       ecto_repos: [PlasmaRepo.Repo]

# Caches config
config :plasma, Plasma.Channels.EventTypeCache,
       # 24 hrs
       gc_interval: 86_400

config :plasma, :matrix,
       default_power_levels: %{
         ban: 50,
         events: %{},
         events_default: 0,
         invite: 50,
         kick: 50,
         redact: 50,
         state_default: 50,
         users: %{},
         users_default: 0,
         notifications: %{}
       }

config :plasma, :matrix,
  supported_room_versions: ~w(1 2 3 4 5),
  default_room_version: "5"

       # Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
