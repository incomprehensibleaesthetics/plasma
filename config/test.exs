use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :plasma, PlasmaWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Limit password hashing cost during tests
config :argon2_elixir, t_cost: 2, m_cost: 8

config :plasma, :matrix,
 auth_flows: %{
   flows: [
     %{
       stages: ["m.login.password"]
     },
     %{
       stages: ["m.login.dummy"]
     }
   ],
   params: %{
     "m.login.password" => %{},
     "m.login.dummy" => %{"dummy_key" => "dummy_value"}
   },
   # 10 minutes
   session_ttl: 600
 }

# Configure your database
config :plasma, PlasmaRepo.Repo,
       username: System.get_env("POSTGRES_USER") || "postgres",
       password: System.get_env("POSTGRES_PASSWORD") || "postgres",
       database: System.get_env("POSTGRES_DB") || "plasma_test",
       hostname: System.get_env("POSTGRES_HOST") || "localhost",
       port: System.get_env("POSTGRES_PORT") || 5432,
       pool: Ecto.Adapters.SQL.Sandbox

