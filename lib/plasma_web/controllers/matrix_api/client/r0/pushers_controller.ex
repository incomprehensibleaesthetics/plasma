defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.Pushers do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  alias PlasmaRepo.Channels.Identifier
  require Logger

  def get_pushers(conn, _params) do
    conn |> json(%{})
  end

end