defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.Account do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  require Logger

  def whoami(conn, _params) do
    with {:ok, user_id} <- Map.fetch(conn.assigns, :user_id),
      user when not is_nil(user) <- PlasmaRepo.Users.get_with_devices(user_id) do
      conn |> json(%{"user_id" => user.mx_user_id})
    else
      _ -> conn |> json_error(:m_unknown_token)
    end
  end
end