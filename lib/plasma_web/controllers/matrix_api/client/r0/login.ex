defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.Login do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  alias PlasmaRepo.Users.AccessTokens
  require Logger

  def get(conn, _params) do
    # TODO: Don't hardcode this response
    # when [login auth flow](https://matrix.org/docs/spec/client_server/r0.4.0.html#id212) will be clarified
    json(conn, %{flows: [%{type: "m.login.password"}]})
  end

  def login(conn, %{"type" => "m.login.password"} = params) do
    do_password_login(conn, params)
  end

  def login(conn, _params) do
    conn |> json_error(:m_unknown)
  end

  def logout(conn, _params) do
    with {:ok, device_id} <- Map.fetch(conn.assigns, :device_id),
         _ <- PlasmaRepo.Users.delete_device(device_id) do
      conn |> json(%{})
    else
      _ -> conn |> json_error(:p_internal_error)
    end
  end

  def logout_all(conn, _params) do
    with {:ok, user_id} <- Map.fetch(conn.assigns, :user_id),
         _ <- PlasmaRepo.Users.delete_user_devices(user_id) do
      conn |> json(%{})
    else
      _ -> conn |> json_error(:p_internal_error)
    end
  end

  defp do_password_login(
         conn,
         %{"identifier" => %{"type" => "m.id.user", "user" => user}} = params
       ) do
    # Check if the user field is fully qualified. If not, just qualify it with local domain
    user_id =
      case PlasmaRepo.Channels.Identifier.parse(user) do
        :error -> PlasmaRepo.Channels.Identifier.new(:user, user)
        {:ok, id} -> id
      end

    password = Map.get(params, "password")
    ua = PlasmaWeb.Tools.get_ua(conn)

    initial_display_name =
      Map.get(params, "initial_display_name", "#{ua} on #{ua.os}/#{ua.device}")

    device_id = Map.get(params, "device_id", PlasmaWeb.Tools.generate_device_id(ua))

    case PlasmaRepo.Users.get_user_check_password(to_string(user_id), password) do
      {:ok, user} ->
        Logger.debug("user login: #{inspect(user)}")
        # Fetch device/token or create new one
        case find_device(user.devices, device_id) do
          nil ->
            # Device not found, create a new one
            with {:ok, device} <-
                   PlasmaRepo.Users.add_device(%{
                     "user_id" => user.id,
                     "mx_device_id" => device_id,
                     "display_name" => initial_display_name
                   }),
                 {:ok, token} <- AccessTokens.encode_token(device, user) do
              conn
              |> json(%{
                "access_token" => token,
                "user_id" => user.mx_user_id,
                "device_id" => device.mx_device_id
              })
            else
              {:error, errors} ->
                send_changeset_error_to_json(conn, errors)
            end

          device ->
            conn
            |> json(%{
              "access_token" => AccessTokens.get_encoded_token(device.access_token.id),
              "user_id" => user.mx_user_id,
              "device_id" => device.mx_device_id
            })
        end

      {:error, :login_failed} ->
        conn |> json_error(:m_forbidden)

      {:error, errors} ->
        send_changeset_error_to_json(conn, errors)

      other_error ->
        Logger.warn("Unhandled error from user login: #{inspect(other_error)}")
        conn |> json_error(:p_internal_error)
    end
  end

  defp do_password_login(conn, _params) do
    conn |> json_error(:m_unknown)
  end

  # Find a device by its matrix ID in a list of devices
  defp find_device(devices, mx_device_id) do
    Enum.find(devices, fn d -> d.mx_device_id == mx_device_id end)
  end
end
