defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.Room do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  alias PlasmaHS.RoomServer.Domain.CreateRoomRequest
  import PlasmaWeb.Errors
  require Logger

  @default_visibility "private"
  @matrix_config Application.get_env(:plasma, :matrix)
  @supported_room_versions @matrix_config[:supported_room_versions]
  @default_room_version @matrix_config[:default_room_version]

  def create_room(conn, params) do
    {:ok, user_id} = Map.fetch(conn.assigns, :user_id)
    user = PlasmaRepo.Users.get(user_id)

    request_room_version = Map.get(params, "room_version", @default_room_version)
    request_visibility = Map.get(params, "visibility", @default_visibility)
    default_creation_content = %{
      creator: user.mx_user_id,
      "m.federate": true,
      room_version: @default_room_version
    }
    with {:room_version, true} <- {:room_version, Enum.member?(@supported_room_versions, request_room_version)},
         {:room_visibility, true} <- {:room_visibility, Enum.member?(["private", "public"], request_visibility)} do
      room_attrs = Map.merge(params, %{
          visibility: request_visibility,
          room_version: request_room_version,
          creation_content: Map.get(params, "creation_content", default_creation_content),
          power_level_content_override: Map.get(params, "power_level_content_override", %{}),
          initial_state: Map.get(params, "initial_state", %{}),
          sender_id: user_id
        })
      with {:ok, state, pid} <- PlasmaHS.RoomServer.create_room(room_attrs) do
        conn |> json(%{"room_id" => state.mx_room_id})
      else
        {:rejected, state} -> conn |> json_error(:m_invalid_room_state)
        {:error, changeset} -> send_changeset_error_to_json(conn, changeset)
      end
    else
      {:room_version, false} -> conn |> json_error(:m_unsupported_room_version)
      {:room_visibility, false} -> conn |> json_error({:m_bad_type, "visibility"})
    end

  end

  def get_keys(conn,_) do
    conn |> json_error(:p_not_implemented)
  end
end