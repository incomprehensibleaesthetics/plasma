defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.Register do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  import PlasmaWeb.Errors
  require Logger

  alias PlasmaRepo.Users
  alias PlasmaRepo.Users.AccessTokens

  def register(conn, params) do
    ua = PlasmaWeb.Tools.get_ua(conn)

    default_params = %{
      "inhibit_login" => false,
      "kind" => "user",
      "mx_device_id" => Map.get(params, "device_id") || PlasmaWeb.Tools.generate_device_id(ua),
      "initial_device_display_name" => "#{ua} on #{ua.os}/#{ua.device}"
    }

    params =
      case Map.get(params, "username") do
        nil ->
          Map.put(params, "mx_user_id", to_string(PlasmaRepo.Channels.Identifier.generate(:user)))

        username ->
          Map.put(
            params,
            "mx_user_id",
            to_string(PlasmaRepo.Channels.Identifier.new(:user, username))
          )
      end

    registration_params = Map.merge(default_params, params)

    Logger.debug("Registration request: #{inspect(registration_params)}")

    with {:ok, user_with_device} <- Users.register_user(registration_params) do
      Logger.debug("user insert result: #{inspect(user_with_device)}")

      device = user_with_device.devices |> List.first()

      json_resp = %{
        "user_id" => user_with_device.mx_user_id
      }

      json_resp =
        if Map.get(registration_params, "inhibit_login", false) == false do
          case AccessTokens.encode_token(device, user_with_device) do
            {:ok, token} ->
              json_resp
              |> Map.merge(%{
                "access_token" => token,
                "device_id" => device.mx_device_id
              })

            error ->
              Logger.warn("Access token encode error: #{error}")
              json_resp
          end
        else
          json_resp
        end

      conn |> json(json_resp)
    else
      {:error, errors} ->
        Logger.debug("registration error detail: #{inspect(errors)}")
        send_changeset_error_to_json(conn, errors)
    end
  end

  def available(%{body_params: %{"username" => username}} = conn, _params) do
    user_id = PlasmaRepo.Channels.Identifier.new(:user, username)

    if PlasmaRepo.Channels.Identifier.valid?(user_id) do
      case Users.get_by_mx_user_id_with_devices(to_string(user_id)) do
        nil -> conn |> json(%{"available" => true})
        _ -> conn |> json_error(:m_user_in_use)
      end
    else
      conn |> json_error(:m_invalid_username)
    end
  end

  def available(conn, _params) do
    conn |> json_error(:m_invalid_username)
  end
end
