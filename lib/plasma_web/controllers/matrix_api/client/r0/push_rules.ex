defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.PushRules do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  alias PlasmaRepo.Channels.Identifier
  alias PlasmaRepo.Users
  alias PlasmaRepo.Users.User
  require Logger

  @matrix_config Application.get_env(:plasma, :matrix)
  @default_content_rules @matrix_config[:default_content_rules]
  @default_override_rules @matrix_config[:default_override_rules]

  def get_push_rules(conn, _params) do
    with %{user_id: user_id} <- conn.assigns,
          %User{} = user <- Users.get_with_devices(user_id),
         {:ok, user_identifier} <- Identifier.parse(user.mx_user_id) do
      conn |> json(%{
        "global" => %{
          "content" => personalize_rules(@default_content_rules, user_identifier),
          "override" => personalize_rules(@default_override_rules, user_identifier),
          "room" => [],
          "sender" => [],
          "underride" => []
        }
      })
    else
      errors ->
        Logger.error("Invalid user_id stored in connection assigns: #{inspect conn.assigns}, #{errors}")
        conn |> json_error(:p_internal_error)
    end
  end

  defp personalize_rules(rules_list, user_identifier) do
    rules_list |> Enum.map(fn rule ->
      case rule do
        %{"rule_id" => ".m.rule.contains_user_name"} -> %{rule | "pattern" => user_identifier.localpart}
        %{"rule_id" => ".m.rule.invite_for_me"} ->
                       %{rule | "conditions" => rule
                                 |> Map.get("conditions")
                                            |> Enum.map(fn condition ->
                         case condition do
                          %{"key" => "state_key",  "kind" => "event_match"} -> %{condition | "pattern" => user_identifier}
                           c -> c
                         end
                       end)}
        rule -> rule
      end
    end)
  end
end