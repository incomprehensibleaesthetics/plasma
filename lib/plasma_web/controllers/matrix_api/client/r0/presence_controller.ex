defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.PresenceController do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  require Logger
  alias PlasmaRepo.Users
  alias PlasmaRepo.Presence
  alias PlasmaRepo.Presence.State

  @doc """
  Handles PUT requests to /_matrix/client/r0/presence/:userId/status
  """
  def update(conn, %{"userId" => user_id, "presence" => presence, "status_msg" => status_msg}) do
    user = Users.get(conn.assigns.user_id)

    if user.mx_user_id != user_id do
      conn |> json_error(:m_forbidden)
    else
      {:ok, agent} = Presence.get(user_id)
      State.put(agent, %{presence: presence, status_msg: status_msg, last_active_ago: 0})
      conn |> json(%{})
    end
  end

  def update(conn, %{"userId" => user_id, "presence" => presence}) do
    user = Users.get(conn.assigns.user_id)

    if user.mx_user_id != user_id do
      conn |> json_error(:m_forbidden)
    else
      {:ok, agent} = Presence.get(user_id)
      State.put(agent, %{presence: presence, last_active_ago: 0})
      conn |> json(%{})
    end
  end

  def update(conn, _params) do
    conn |> json_error({:m_bad_type, "presence"})
  end

  @doc """
  Handles GET requests to /_matrix/client/r0/presence/:userId/status
  """
  def show(conn, %{"userId" => user_id}) do
    Logger.debug("presence requested for '#{user_id}'")

    with {:ok, presence_agent} <- Presence.get(user_id),
         presence <- State.get(presence_agent) do
      conn |> json(presence)
    else
      :error -> conn |> json_error(:m_not_found)
    end
  end
end
