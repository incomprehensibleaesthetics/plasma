defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.DeviceController do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  alias PlasmaRepo.Users.Devices
  require Logger

  def index(conn, _params) do
    devices =
      conn.assigns.user_id
      |> Devices.get_all_by_user_id()

    render(conn, "index.json", devices: devices)
  end

  def show(conn, %{"deviceId" => mx_device_id}) do
    device = Devices.get_by_mx_device_id(mx_device_id)

    case device do
      nil ->
        conn
        |> json_error(:m_not_found)

      _ ->
        render(conn, "show.json", device: device)
    end
  end

  def update(conn, %{"deviceId" => mx_device_id, "display_name" => new_display_name}) do
    with device when not is_nil(device) <- Devices.get_by_mx_device_id(mx_device_id),
         {:ok, _} <- Devices.update_display_name(device, new_display_name) do
      conn |> json(%{})
    else
      nil ->
        conn |> json_error(:m_not_found)

      {:error, _} ->
        conn |> json_error(:m_unknown)
    end
  end
end
