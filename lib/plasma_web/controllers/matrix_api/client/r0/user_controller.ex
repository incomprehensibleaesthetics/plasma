defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.UserController do
  use PlasmaWeb, :controller
  use PlasmaWeb.Controllers.MatrixApi.MatrixController
  alias PlasmaRepo.Users
  import PlasmaWeb.Errors
  require Logger

  def post_filter(conn, %{"userId" => userId}) do
    case Users.create_filter(userId, conn.body_params) do
      {:ok, filter} -> conn |> json(%{"filter_id" => filter.filter_id})
      {:error, errors} -> send_changeset_error_to_json(conn, errors)
    end
  end

  def get_filter(conn, %{"userId" => userId, "filterId" => filterId}) do
    case Users.get_filter(userId, filterId) do
      nil -> conn |> put_status(404)
      filter -> conn |> json(filter.query)
    end
  end
end