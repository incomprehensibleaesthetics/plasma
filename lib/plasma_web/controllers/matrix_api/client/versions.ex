defmodule PlasmaWeb.Controllers.MatrixApi.Client.Versions do
  use PlasmaWeb, :controller

  def get(conn, _params) do
    json(conn, %{versions: ["r0.4.0", "r0.5.0", "r0.6.0"]})
  end
end
