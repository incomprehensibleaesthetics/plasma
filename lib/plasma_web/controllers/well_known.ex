defmodule PlasmaWeb.Controllers.WellKnown do
  use PlasmaWeb, :controller

  @homeserver_base_url Application.get_env(:plasma, :homeserver_base_url)
  @identity_server Application.get_env(:plasma, :identity_server)

  def get(conn, _params) do
    json(
      conn,
      %{
        "m.homeserver" => %{base_url: @homeserver_base_url},
        "m.identity_server" => %{base_url: @identity_server}
      }
    )
  end
end
