defmodule PlasmaWeb.Guardian do
  use Guardian, otp_app: :plasma

  def subject_for_token(access_token, _claims) do
    {:ok, to_string(access_token.id)}
  end

  def resource_from_claims(%{"sub" => access_token_id}) do
    {:ok, access_token_id}
  end

  def resource_from_claims(_claims) do
    {:error, :no_claims_sub}
  end
end
