defmodule PlasmaWeb.Tools do
  # Returns a readable version of UA
  def get_ua_repr(conn) do
    ua = PlasmaWeb.Tools.get_ua(conn)
    "#{ua} on #{ua.os}/#{ua.device}"
  end

  def get_ua(conn) do
    conn
    |> Plug.Conn.get_req_header("user-agent")
    |> List.first()
    |> UAParser.parse()
  end

  def generate_device_id(ua) do
    "#{ua}/#{ua.os}" |> Base.url_encode64(padding: false, ignore: :whitespace)
  end
end
