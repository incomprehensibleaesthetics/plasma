defmodule PlasmaWeb.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @domain Application.get_env(:plasma, :domain)
  @system_username "__PLASMA__"

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      {Horde.Registry, [name: Plasma.PlasmaRegistry, keys: :unique]},
      {Horde.Supervisor,
       [
         name: PlasmaRepo.PlasmaSupervisor,
         strategy: :one_for_one,
         max_restarts: 100_000,
         max_seconds: 1
       ]},
      {Task.Supervisor, name: PlasmaRepo.TaskSupervisor},
      PlasmaWeb.Plugs.AuthSessionCache,
      PlasmaWeb.Plugs.AuthSessionCache.Primary,
      # Start the endpoint when the application starts
      PlasmaWeb.Endpoint,
      PlasmaRepo.Events.EventTypeCache,
      PlasmaRepo.Users.AccessTokenCache,
      # Start the Ecto repository
      PlasmaRepo.Repo,
      # Start presence management
      {PlasmaRepo.Presence, [name: :presence]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: PlasmaWeb.Supervisor]
    supervisor = Supervisor.start_link(children, opts)

    supervisor
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PlasmaWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  def domain() do
    @domain
  end

  def system_user() do
    PlasmaRepo.Channels.Identifier.new(:user, @system_username)
  end
end
