defmodule PlasmaWeb.Plugs.InteractiveAuthFlow do
  require Logger
  alias Plug.Conn
  alias PlasmaWeb.Plugs.AuthSessionCache
  alias Plasma.Utils.Randomizer
  import Phoenix.Controller, only: [json: 2]

  @matrix_config Application.get_env(:plasma, :matrix)
  @auth_flows_config @matrix_config[:auth_flows]
  @flows @auth_flows_config[:flows]
  @params @auth_flows_config[:params]
  @session_ttl @auth_flows_config[:session_ttl]

  @type auth_session() :: %{
          id: String.t(),
          completed_stages: [String.t()],
          auth_completed: boolean()
        }

  def init(options), do: options

  def call(conn, _options) do
    case Map.get(conn.assigns, :access_token) do
      # No access token detected, proceed to interactive API
      nil -> interactive_flow(conn)
      # Passthrough if access_token is set
      _token -> conn
    end
  end

  defp interactive_flow(conn) do
    case get_auth_session(conn) do
      nil ->
        build_response(create_auth_session(), conn, 401)

      session ->
        session |> check_completed_stage(conn) |> build_response(conn, 401)
    end
  end

  # given a auth session and the "auth" body passed by the client
  # this function tries to complete one step of any stages
  # if successful, updates the complete_stages list until this list match one stage
  defp check_completed_stage(session, conn) do
    case auth = Map.get(conn.body_params, "auth") do
      %{"type" => type} ->
        check_auth(type, auth, session) |> check_auth_completed() |> update_auth_session_cache()

      _ ->
        session
    end
  end

  # Check "auth" body for m.login.dummy auth type
  # If simply unconditionnally add "m.login.dummy" to the completed stages list
  defp check_auth(type, _auth, session) when type == "m.login.dummy" do
    Logger.debug("Completed stage m.login.dummy")
    %{session | completed_stages: session.completed_stages ++ ["m.login.dummy"]}
  end

  # Default auth
  defp check_auth(type, _auth, session) do
    Logger.warn("Authentication type '#{type}' is not supported")
    session
  end

  # Check if any stage is complete
  # Test is made by substracting each stages with the completed stage list
  # if any substraction gives an empty list, if means that the completed list contains all the steps of
  # the given stage, so the stage is complete and auth is granted
  defp check_auth_completed(session) do
    check =
      Enum.map(@flows, fn flow ->
        flow.stages -- session.completed_stages
      end)
      |> Enum.find(fn stages -> length(stages) == 0 end)

    if(check != nil) do
      %{session | auth_completed: true}
    else
      session
    end
  end

  @doc """
  Find an existing auth session in cache from the body request
  """
  def get_auth_session(conn) do
    with auth when not is_nil(auth) <- Map.get(conn.body_params, "auth"),
         session_id when not is_nil(session_id) <- Map.get(auth, "session"),
         session when not is_nil(session) <- AuthSessionCache.get(session_id) do
      Logger.debug("Found existing auth session with id #{session_id}")
      session
    else
      nil -> nil
    end
  end

  # Create a new session struct and store it to the cache
  defp create_auth_session() do
    session_id = Randomizer.crypto_random_string()
    session = %{id: session_id, completed_stages: [], auth_completed: false}
    AuthSessionCache.set(session_id, session, ttl: @session_ttl)
  end

  # Update sessions cache
  defp update_auth_session_cache(session) do
    case session.auth_completed do
      false ->
        AuthSessionCache.set(session.id, session, ttl: @session_ttl)

      true ->
        AuthSessionCache.delete(session.id)
        session
    end
  end

  defp build_response(session, conn, status) do
    if !session.auth_completed do
      conn
      |> Conn.put_status(status)
      |> json(%{
        "flows" => @flows,
        "params" => @params,
        "session" => session.id,
        "completed" => session.completed_stages
      })
      |> Conn.halt()
    else
      conn
    end
  end
end
