defmodule PlasmaWeb.Controllers.MatrixApi.Client.R0.DeviceView do
  use PlasmaWeb, :view

  def render("index.json", %{devices: devices}) do
    mapped_devices =
      devices
      |> Enum.map(fn device ->
        %{
          device_id: device.mx_device_id,
          display_name: device.display_name,
          last_seen_ip: device.last_seen_ip,
          last_seen_ts: last_seen_ts(device)
        }
      end)

    %{devices: mapped_devices}
  end

  def render("show.json", %{device: device}) do
    %{
      device_id: device.mx_device_id,
      display_name: device.display_name,
      last_seen_ip: device.last_seen_ip,
      last_seen_ts: last_seen_ts(device)
    }
  end

  defp last_seen_ts(device) do
    if device.last_seen_ts do
      DateTime.to_unix(device.last_seen_ts, :millisecond)
    else
      nil
    end
  end
end
