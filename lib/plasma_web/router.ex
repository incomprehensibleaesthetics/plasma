defmodule PlasmaWeb.Router do
  use PlasmaWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug PlasmaWeb.Plugs.ExtractAccessToken
    plug PlasmaWeb.Plugs.RequireAccessToken
  end

  pipeline :interactive_auth do
    plug PlasmaWeb.Plugs.InteractiveAuthFlow
  end

  scope "/.well-known", PlasmaWeb.Controllers do
    get "/matrix/client", WellKnown, :get
  end

  scope "/_matrix/client", PlasmaWeb.Controllers.MatrixApi.Client do
    pipe_through :api

    get "/versions", Versions, :get

    scope "/r0", R0 do
      get "/login", Login, :get
      post "/login", Login, :login
      get "/register/available", Register, :available
      get "/sync", SyncController, :get_sync
    end
  end

  scope "/_matrix/client", PlasmaWeb.Controllers.MatrixApi.Client do
    pipe_through [:api, :interactive_auth]

    scope "/r0", R0 do
      post "/register", Register, :register
    end
  end

  scope "/_matrix/client", PlasmaWeb.Controllers.MatrixApi.Client do
    pipe_through [:api, :auth]

    scope "/r0", R0 do
      get "/capabilities", CapabilitiesController, :get_capabilities
      post "/logout", Login, :logout
      post "/logout/all", Login, :logout_all
      post "/account/whoami", Account, :whoami
      post "/createRoom", Room, :create_room
      get "/room_keys/version", Room, :get_keys

      scope "/pushrules" do
        get "/", PushRules, :get_push_rules
      end

      scope "/pushers" do
        get "/", Pushers, :get_pushers
      end

      scope "/user" do
        post "/:userId/filter", UserController, :post_filter
        get "/:userId/filter/:filterId", UserController, :get_filter
      end

      scope "/presence" do
        put "/:userId/status", PresenceController, :update
        get "/:userId/status", PresenceController, :show
      end

      scope "/devices" do
        get "/", DeviceController, :index
        get "/:deviceId", DeviceController, :show
        put "/:deviceId", DeviceController, :update
        delete "/:deviceId", DeviceController, :delete
        post "/delete_devices", DeviceController, :delete
      end
    end
  end
end
