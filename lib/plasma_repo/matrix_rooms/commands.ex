defmodule PlasmaRepo.MatrixRooms.Commands do
  defmodule CreateRoom do
    @enforce_keys [:sender, :visibility, :room_version, :power_level_content_override]
    defstruct [:sender, :visibility, :room_version, :creation_content, :power_level_content_override]
  end

  defmodule ChangeJoinRules do
    @enforce_keys [:sender, :join_rule]
    defstruct [:sender, :join_rule]
  end

  defmodule AddStateEvent do
    @enforce_keys [:sender, :type, :state_key]
    defstruct [:sender, :type, :state_key, :content]
  end
end