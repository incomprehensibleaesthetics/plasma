defmodule PlasmaRepo.MatrixRooms.PowerLevels do
  require Logger

  #Check if a user has required power levels for event
  # true if the event required level is <= to the user power level
  # false otherwise
  def user_has_sufficient_power_level(event, user_id, state) do
    required = required_power_level_for_event(event, state)
    user_power = user_power_level(user_id, state)
    Logger.debug("Required power level for event #{event.type_name}=#{required} ; #{user_id} power_level=#{user_power}")
    required <= user_power
  end

  defp required_power_level_for_event(event, state) do
    #If an event type is specified in events, then the user must have at least the level specified in order to send that event.
    #If the event type is not supplied, it defaults to events_default for Message Events and state_default for State Events.
    state
    |> Map.get(:power_levels, %{})
    |> Map.get(:events, %{})
    |> Map.get(event.type_name, default_level_for_event(event, state))
  end

  #Determine the required level for given event.
  #If it's a state event, state_default value is returned, else events_default is returned
  #This corresponds to the phrase "defaults to events_default for Message Events and state_default for State Events." from §§12.5.6 C2S
  #State events are supposed to have a :state key
  defp default_level_for_event(event, state) do
    {state_default, events_default} = case state.power_levels do
      nil -> {0, 0} #no m.room.power_levels event, both the state_default and events_default are 0.
      power_levels ->
        #If there is no state_default in the m.room.power_levels event, the state_default is 50.
        #If there is no events_default in the m.room.power_levels event, the events_default is 0.
        {Map.get(power_levels, :state_default, 50), Map.get(power_levels, :state_default, 0)}
    end
    if Map.has_key?(event, :state) do
      state_default
    else
      events_default
    end
  end

  defp user_power_level(user_id, state) do
    case state.power_levels do
      nil ->
        #If the room contains no m.room.power_levels event, the room's creator has a power level of 100, and all other users have a power level of 0.
        if user_id == state.creator do
          100
        else
          0
        end
      power_levels ->
        #If a user_id is in the users list, then that user_id has the associated power level.
        #Otherwise they have the default level users_default. If users_default is not supplied, it is assumed to be 0.
        users_default = Map.get(power_levels, :users_default, 0)
        power_levels |> Map.get(:users, %{}) |> Map.get(user_id, users_default)
    end
  end
end