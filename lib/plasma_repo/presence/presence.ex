defmodule PlasmaRepo.Presence do
  use GenServer
  alias PlasmaRepo.Presence.State

  @doc """
  Starts presence management
  """
  @spec start_link(list) :: {:ok, pid}
  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @doc """
  Gets the presence for a user
  """
  @spec get(String.t()) :: {:ok, pid}
  def get(user_id) do
    GenServer.call(:presence, {:get, user_id})
  end

  @doc """
  Creates a new presence for a user
  """
  @spec create(String.t()) :: {:ok, pid}
  def create(user_id) do
    GenServer.call(:presence, {:create, user_id})
  end

  @impl true
  def init(:ok) do
    {:ok, %{}}
  end

  @impl true
  def handle_call({:get, user_id}, _from, users) do
    with {:ok, agent} <- Map.fetch(users, user_id) do
      {:reply, {:ok, agent}, users}
    else
      :error ->
        {:ok, agent} = State.start_link([])
        {:reply, {:ok, agent}, Map.put(users, user_id, agent)}
    end
  end

  @impl true
  def handle_call({:create, user_id}, _from, users) do
    if Map.has_key?(users, user_id) do
      {:reply, Map.fetch(users, user_id), users}
    else
      {:ok, agent} = State.start_link([])
      {:reply, {:ok, agent}, Map.put(users, user_id, agent)}
    end
  end
end
