defmodule PlasmaRepo.Presence.State do
  alias __MODULE__
  use Agent
  require Logger

  @type t :: %{
          presence: :offline | :unavailable | :online,
          last_active_ago: non_neg_integer,
          status_msg: String.t()
        }

  @derive Jason.Encoder
  @enforce_keys [:presence]
  defstruct [:presence, :status_msg, :last_active_ago]

  @doc """
  Starts a new presence state agent
  """
  @spec start_link(list) :: {:ok, pid}
  def start_link(opts) do
    Agent.start_link(
      fn ->
        %State{
          presence: "offline",
          last_active_ago: 0
        }
      end,
      opts
    )
  end

  @doc """
  Gets the current presence state
  """
  @spec get(pid) :: t
  def get(pid) do
    Agent.get(pid, fn state -> state end)
  end

  @doc """
  Updates the current presence state
  """
  @spec put(pid, map) :: :ok
  def put(pid, state) do
    Enum.each(state, fn {key, value} -> Agent.update(pid, &Map.put(&1, key, value)) end)
  end
end
