defmodule PlasmaRepo.Users.AccessTokens do
  require Logger
  require Ecto.Query
  alias PlasmaRepo.Repo
  alias PlasmaRepo.Users.AccessToken
  alias PlasmaRepo.Users.{User, Device}

  @cache_ttl 60*60 #1 hour

  @spec encode_token(Device.t(), User.t()) :: {:ok, String.t()} | {:error, Ecto.Changeset.t()}
  def encode_token(device, user) do
    with {:ok, access_token} <-
           %AccessToken{}
           |> AccessToken.create_changeset(%{device_id: device.id, user_id: user.id})
           |> Repo.insert,
          access_token <- Repo.preload(access_token, [:device, :user]),
         {:ok, token, _claims} <- PlasmaWeb.Guardian.encode_and_sign(access_token) do
      PlasmaRepo.Users.AccessTokenCache.set(access_token.id, access_token, ttl: @cache_ttl)
      {:ok, token}
    end
  end

  def decode_token(encoded_token) do
    with {:ok, access_token_id, _claims} when not is_nil(encoded_token) <- PlasmaWeb.Guardian.resource_from_token(encoded_token),
         access_token when not is_nil(access_token_id) <- PlasmaRepo.Users.AccessTokenCache.get(access_token_id) || Repo.get(AccessToken, access_token_id) do
      if access_token != nil do
        PlasmaRepo.Users.AccessTokenCache.add(access_token.id, access_token, ttl: @cache_ttl)
      end
      access_token
    else
      _ -> nil
    end
  end

  def get_encoded_token(access_token_id) do
    case PlasmaRepo.Users.AccessTokenCache.get(access_token_id) || Repo.get(AccessToken, access_token_id) do
      nil -> nil
      access_token -> PlasmaWeb.Guardian.encode_and_sign(access_token)
    end
  end
end
