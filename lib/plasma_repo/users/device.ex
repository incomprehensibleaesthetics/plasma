defmodule PlasmaRepo.Users.Device do
  require Logger
  use PlasmaRepo.Schema

  @type t :: %__MODULE__{
          mx_device_id: String.t(),
          display_name: String.t(),
          user: User.t(),
          user_id: String.t(),
          access_token: AccessToken.t(),
          last_seen_ip: String.t(),
          last_seen_ts: pos_integer
        }

  schema "devices" do
    field :mx_device_id, :string
    field :display_name, :string
    field :last_seen_ip, :string
    field :last_seen_ts, :utc_datetime
    belongs_to :user, PlasmaRepo.Users.User
    has_one :access_token, PlasmaRepo.Users.AccessToken
    timestamps()
  end

  def changeset(device, attrs \\ %{}) do
    device
    |> cast(attrs, [:user_id, :mx_device_id, :display_name, :last_seen_ip, :last_seen_ts])
    |> validate_required([:mx_device_id])
    |> assoc_constraint(:user)
    |> unique_constraint(:mx_device_id, message: "device_already_exists")
  end

  def create_changeset(device, attrs \\ %{}) do
    device
    |> changeset(attrs)
  end

  def update_changeset(device, attrs \\ %{}) do
    device
    |> cast(attrs, [:display_name])
  end
end
