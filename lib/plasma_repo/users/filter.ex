defmodule PlasmaRepo.Users.Filter do
  use PlasmaRepo.Schema

  schema "filters" do
    field :filter_id, :string
    field :user_id, :string
    field :query, :map
    timestamps()
  end

  def create_changeset(filter, attrs \\ %{}) do
    filter
    |> cast(attrs, [:filter_id, :user_id, :query])
    |> validate_required([:filter_id, :user_id, :query], message: "required")
    |> unique_constraint(:filter_id, name: "filters_filter_id_user_id_index")
  end
end
