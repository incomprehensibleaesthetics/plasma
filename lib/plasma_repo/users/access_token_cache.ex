defmodule PlasmaRepo.Users.AccessTokenCache do
  use Nebulex.Cache,
    otp_app: :plasma,
    adapter: Nebulex.Adapters.Local
end
