defmodule PlasmaRepo.Users.Devices do
  require Logger
  import Ecto.Query
  alias PlasmaRepo.Repo
  alias PlasmaRepo.Users.Device

  @doc """
  Returns all devices associated with a user
  """
  @spec get_all_by_user_id(String.t()) :: [Device.t()]
  def get_all_by_user_id(user_id) when is_nil(user_id) or byte_size(user_id) == 0, do: []

  def get_all_by_user_id(user_id) do
    Repo.all(Device, user_id: user_id)
  end

  @doc """
  Returns a device by mx_device_id
  """
  @spec get_by_mx_device_id(String.t()) :: Device.t() | nil
  def get_by_mx_device_id(mx_device_id) do
    Repo.one(from d in Device, where: d.mx_device_id == ^mx_device_id)
  end

  @doc """
  Updates the display name of the given device
  """
  @spec update_display_name(Device.t(), String.t()) :: {:ok, Device.t()} | {:error, Chageset.t()}
  def update_display_name(device, display_name) do
    device
    |> Device.update_changeset(%{"display_name" => display_name})
    |> Repo.update()
  end
end
