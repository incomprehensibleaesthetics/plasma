defmodule PlasmaRepo.Events.EventAuth do
  use PlasmaRepo.Schema
  alias PlasmaRepo.Events.Event

  @type t :: %__MODULE__{
               event: Event.t(),
               auth_event: Event.t()
             }

  @primary_key false
  schema "event_auths" do
    belongs_to :event, Event
    belongs_to :auth_event, Event
  end

  def changeset(event_edge, attrs \\ %{}) do
    event_edge
    |> cast(attrs, [:event_id, :auth_event_id])
    |> validate_required([:event_id, :auth_event_id], message: "required")
    |> assoc_constraint(:event)
    |> assoc_constraint(:auth_event)
  end

  def create_changeset(event_edge, attrs \\ %{}) do
    event_edge |> changeset(attrs)
  end
end