defmodule PlasmaRepo.Events do
  require Logger
  alias PlasmaRepo.Repo
  alias PlasmaRepo.Events.{Event, EventEdge, EventType, EventTypeCachableRepo}

  def create_event(attrs, parents \\ []) do
    Ecto.Multi.new
    |> create_event_in_transaction(attrs, parents)
    |> Repo.transaction()
  end

  def create_event_in_transaction(multi, attrs, parents \\ []) do
    with m <- Ecto.Multi.insert(multi, :event, Event.create_changeset(%Event{}, attrs)) do
      Enum.reduce(parents, m, fn parent, multi ->
        Ecto.Multi.insert(m, :event_edge, fn %{event: event} ->
          EventEdge.create_changeset(%EventEdge{}, %{child_event_id: event.id, parent_event_id: parent.id})
        end )
      end)
      m
    end
  end

  @spec get_or_create_event_type(String.t()) :: {:ok, EventType.t()} | {:error, term()}
  def get_or_create_event_type(type_name) do
    case get_event_type_by_name(type_name) do
      nil ->
        create_event_type(type_name)

      {:error, cause} ->
        Logger.warn("EventTypeCache get error: #{cause}")
        {:error, cause}

      event_type ->
        {:ok, event_type}
    end
  end

  def get_event_type_by_name(type_name) do
    EventTypeCachableRepo.get_by(EventType, name: type_name)
  end

  def get_event_type_by_id(id) do
    EventTypeCachableRepo.get(EventType, id)
  end

  def create_event_type(type_name) do
    EventTypeCachableRepo.insert(EventType.create_changeset(%EventType{}, %{name: type_name}))
  end

  @doc """
  Generate en event ID compatible with the given room version
  """
  @spec gen_mx_event_id(String.t()) :: String.t()
  def gen_mx_event_id(room_version) do
    case room_version do
      version when version in ["1", "2"] -> to_string(PlasmaRepo.Channels.Identifier.generate(:event))
      _ -> "$" <> Plasma.Utils.Randomizer.unique_id() #TODO: This is not so simple. ID should use event hash
    end
  end
end