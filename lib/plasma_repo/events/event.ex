defmodule PlasmaRepo.Events.Event do
  use PlasmaRepo.Schema

  alias PlasmaRepo.Events.{Event, EventContent, EventEdge, EventAuth}
  alias PlasmaRepo.Events.EventType
  alias PlasmaRepo.Events.Identifier
  alias PlasmaRepo.Rooms.Room

  @type t :: %__MODULE__{
          mx_event_id: String.t(),
          state_key: String.t(),
          origin_server_ts: DateTime.t(),
          received_ts: DateTime.t(),
          content: map,
          rejected: boolean,
          rejection_cause: String.t(),
          sender: User.t(),
          room: Room.t(),
          event_type: EventType.t(),
          inserted_at: DateTime.t(),
          updated_at: DateTime.t(),
          child_edges: [EventEdge.t()],
          parent_edges: [EventEdge.t()],
          auth_events: [EventAuth.t()]
             }

  schema "events" do
    field :mx_event_id, :string
    field :state_key, :string, default: nil
    field :origin_server_ts, :utc_datetime, virtual: true
    field :received_ts, :utc_datetime, virtual: true
    field :content, :map
    field :rejected, :boolean, default: false
    field :rejection_cause, :string
    belongs_to :sender, PlasmaRepo.Users.User
    belongs_to :room, PlasmaRepo.Rooms.Room
    belongs_to :event_type, EventType
    has_many :child_edges, EventEdge, foreign_key: :child_event_id
    has_many :parent_edges, EventEdge, foreign_key: :parent_event_id
    has_many :auth_events, EventAuth, foreign_key: :event_id
    timestamps()
  end

  def changeset(%Event{} = event, attrs \\ %{}) do
    event
    |> cast(attrs, [
      :mx_event_id,
      :state_key,
      :origin_server_ts,
      :received_ts,
      :sender_id,
      :room_id,
      :event_type_id,
      :rejected,
      :rejection_cause
    ])
    |> validate_required([:sender_id, :room_id, :event_type_id], message: "required")
    |> validate_origin_server_ts()
    |> unique_constraint(:mx_event_id, message: "unique")
    |> assoc_constraint(:sender)
    |> assoc_constraint(:room)
    |> assoc_constraint(:event_type)
  end

  defp validate_origin_server_ts(%Ecto.Changeset{valid?: true} = changeset) do
    case changeset do
      %Ecto.Changeset{changes: %{origin_server_ts: origin_server_ts}} -> changeset
      _ -> Ecto.Changeset.put_change(changeset, :origin_server_ts, DateTime.utc_now())
    end
  end
  defp validate_origin_server_ts(changeset), do: changeset

  def create_changeset(%Event{} = event, attrs \\ %{}) do
    event
    |> changeset(attrs)
  end
end
