defmodule PlasmaRepo.Events.EventType do
  require Logger
  use PlasmaRepo.Schema
  alias PlasmaRepo.Events.EventTypeCachableRepo

  @type t :: %__MODULE__{
               name: String.t(),
               inserted_at: DateTime.t()
             }

  schema "event_types" do
    field :name, :string
    timestamps(updated_at: false)
  end

  def create_changeset(event_type, attrs) do
    event_type
    |> cast(attrs, [:name])
    |> validate_required([:name], message: "required")
    |> unique_constraint(:name, message: "non_unique")
  end
end
