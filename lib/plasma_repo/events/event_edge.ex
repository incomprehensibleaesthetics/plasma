defmodule PlasmaRepo.Events.EventEdge do
  use PlasmaRepo.Schema
  alias PlasmaRepo.Events.Event

  @type t :: %__MODULE__{
               child_event: Event.t(),
               parent_event: Event.t()
             }

  @primary_key false
  schema "event_edges" do
    belongs_to :child_event, Event
    belongs_to :parent_event, Event
  end

  def changeset(event_edge, attrs \\ %{}) do
    event_edge
    |> cast(attrs, [:child_event_id, :parent_event_id])
    |> validate_required([:child_event_id, :parent_event_id], message: "required")
    |> assoc_constraint(:child_event)
    |> assoc_constraint(:parent_event)
  end

  def create_changeset(event_edge, attrs \\ %{}) do
    event_edge |> changeset(attrs)
  end
end