defmodule PlasmaHS.RoomServer.Auth do
  @moduledoc """
  Event authentication functions
  """
  require PlasmaHS.EventTypes
  require Logger

  defmodule Module.concat(PlasmaHS.RoomServer.Auth, "1") do
    defdelegate auth_event(room_state, event), to: Module.concat(PlasmaHS.RoomServer.Auth, "5")
  end
  defmodule Module.concat(PlasmaHS.RoomServer.Auth, "2") do
    defdelegate auth_event(room_state, event), to: Module.concat(PlasmaHS.RoomServer.Auth, "5")
  end
  defmodule Module.concat(PlasmaHS.RoomServer.Auth, "3") do
    defdelegate auth_event(room_state, event), to: Module.concat(PlasmaHS.RoomServer.Auth, "5")
  end
  defmodule Module.concat(PlasmaHS.RoomServer.Auth, "4") do
    defdelegate auth_event(room_state, event), to: Module.concat(PlasmaHS.RoomServer.Auth, "5")
  end

  defmodule Module.concat(PlasmaHS.RoomServer.Auth, "5") do
    @matrix_config Application.get_env(:plasma, :matrix)
    @supported_room_versions @matrix_config[:supported_room_versions]

    def auth_event(room_state, %{event_type: PlasmaHS.EventTypes.m_room_create} = event) do
      with parents when parents == [] <- event.parent_events,
           :ok <- room_domain_match_user_domain(room_state, event),
           :ok <- create_event_content_room_version_supported(room_state, event),
           :ok <- creator_event_content_creator_present(room_state, event) do
        {:ok, event}
      else
        {:rejected, cause} -> {:ok, Map.merge(event, %{rejected: true, rejection_cause: cause})}
      end
    end

    defp creator_event_content_creator_present(_state, %{content: event_content}) do
      case Map.get(event_content, :creator) do
        nil -> {:rejected, "content has no creator field"}
        _ -> :ok
      end
    end

    defp create_event_content_room_version_supported(_state, %{content: event_content}) do
      case Map.get(event_content, "room_version") do
        nil -> :ok
        room_version -> if Enum.member?(@supported_room_versions, room_version) do
                          :ok
                        else
                          {:rejected, "content.room_version is present and is not a recognised version"}
                        end
      end
    end

    defp room_domain_match_user_domain(state, create_event) do
      with %{mx_room_id: mx_room_id} <- state,
          %{sender_id: sender_id} <- create_event,
          user when not is_nil(user) <- PlasmaRepo.Users.get(sender_id),
           {:ok, room_identifier} <- PlasmaRepo.Channels.Identifier.parse(mx_room_id),
           {:ok, user_identifier} <- PlasmaRepo.Channels.Identifier.parse(user.mx_user_id) do
           if room_identifier.domain == user_identifier.domain do
             :ok
           else
             {:rejected, "m.room.create event: domain of the room_id does not match the domain of the sender"}
           end
      else
        _ -> {:rejected, "m.room.create event: domain of the room_id does not match the domain of the sender"}
      end
    end

  end

  @spec auth_event(room_version :: String.t(), room_state :: RoomState, event :: map) :: {:ok, map} | {:rejected, any} | :error
  def auth_event(room_version, room_state, event) do
    module = Module.safe_concat(PlasmaHS.RoomServer.Auth, room_version)
    try do
      apply(module, :auth_event, [room_state, event])
    rescue
      error -> Logger.warn("Error calling auth_event on module #{module}: #{inspect error}")
               {:ok, Map.merge(event, %{rejected: true, rejection_cause: "unsupported room version: '#{room_version}'"})}
    end
  end
end
