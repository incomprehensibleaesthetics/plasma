defmodule PlasmaHS.RoomServer.Domain do

  defmodule RoomState do
    defstruct [:room_id, :mx_room_id, :create_event, :creator, :power_levels, :join_rule, :state_events]
  end
end