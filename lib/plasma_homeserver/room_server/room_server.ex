defmodule PlasmaHS.RoomServer do
  use GenServer
  require PlasmaHS.EventTypes

  alias PlasmaRepo.Rooms
  alias PlasmaHS.RoomServer.RoomState
  alias PlasmaHS.RoomServer.Domain.{CreateRoomRequest, RoomState}

  @spec create_room(map) ::
          {:ok, RoomState.t(), pid}
          | {:error, RoomState.t()}
          | {:error, RoomState.t(), Changeset.t()}
  def create_room(%{} = request) do
    with {:ok, request} <- CreateRoomRequest.validate(request),
         {:ok, room} <-
           PlasmaRepo.Rooms.create_room(%{
             mx_room_id: to_string(PlasmaRepo.Channels.Identifier.generate(:room))
           }),
         {:ok, pid} <- start_room_server(room.id),
         :ok <- GenServer.cast(pid, request),
         state <- get_room_state(pid) do
      # {:ok, _, _state} <- change_preset(pid, request.sender, request.preset, request.visibility) do
      # add_initial_state(pid, request.sender, request.initial_state)
      # state = if not is_nil(request.name) do
      # add_state_event(pid, request.sender, EventTypes.m_room_name, "", request.name)
      # end
      # state = if not is_nil(request.topic) do
      #  add_state_event(pid, request.sender, EventTypes.m_room_topic, "", request.topic)
      # end
      {:ok, state, pid}
    else
      {:error, changeset} -> {:error, changeset}
    end
  end

  def get_room_state(pid) do
    GenServer.call(pid, :get_room_state)
  end

  defp change_preset(channel, sender, preset, visibility) do
    join_rule =
      case preset do
        "private_chat" ->
          "invite"

        "trusted_private_chat" ->
          "invite"

        "public_chat" ->
          "public"

        nil ->
          case visibility do
            "public" -> "public"
            "private" -> "invite"
          end
      end

    GenServer.call(channel, %PlasmaRepo.MatrixRooms.Commands.ChangeJoinRules{
      sender: sender,
      join_rule: join_rule
    })
  end

  defp add_initial_state(channel, sender, initial_state) do
    initial_state
    |> Enum.reduce(nil, fn event, _acc ->
      add_state_event(channel, sender, event.type, event.state_key, event.content)
    end)
  end

  defp add_state_event(channel, sender, type, state_key, content) do
    GenServer.call(channel, %PlasmaRepo.MatrixRooms.Commands.AddStateEvent{
      sender: sender,
      type: type,
      state_key: state_key,
      content: content
    })
  end

  @doc """
  Try to start a room server. If the room is already started, the existing pid is returned
  The room must exists before starting the room server
  """
  @spec start_room_server(String.t()) ::
          {:ok, pid()} | :ignore | {:error, {:already_started, pid()} | term()}
  def start_room_server(room_id) do
    with room when not is_nil(room) <- Rooms.get_room(room_id),
         {:error, {:already_started, pid}} <-
           GenServer.start_link(__MODULE__, [room], name: via_tuple(room.id)) do
      {:ok, pid}
    else
      nil -> {:error, "No room exists in repo with id #{room_id}"}
      {:ok, _} = pid -> pid
    end
  end

  defp via_tuple(room_id), do: {:via, Horde.Registry, {Plasma.PlasmaRegistry, room_id}}

  @impl true
  def init([room]) do
    {:ok,
     %RoomState{
       room_id: room.id,
       mx_room_id: room.mx_room_id,
       state_events: %{}
     }}
  end

  @impl true
  def handle_call(:get_room_state, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_cast(%CreateRoomRequest{} = request, state) do
    with {:ok, new_state} <- create_m_room_create_event(request, state) do
      {:noreply, new_state}
    else
      _ -> {:noreply, state}
    end
  end

  defp create_m_room_create_event(%CreateRoomRequest{} = request, state) do
    event_attrs = %{
      room_id: state.room_id,
      event_type: PlasmaHS.EventTypes.m_room_create(),
      sender_id: request.sender_id,
      state_key: "",
      content: request.creation_content,
      parent_events: [],
      auth_events: []
    }

    with {:ok, authed_event_attrs} <-
           PlasmaHS.RoomServer.Auth.auth_event(request.room_version, state, event_attrs),
         {:ok, create_event_type} <-
           PlasmaRepo.Events.get_or_create_event_type(PlasmaHS.EventTypes.m_room_create()),
         {:ok, %{event: create_event}} =
           PlasmaRepo.Events.create_event(
             Map.merge(authed_event_attrs, %{event_type_id: create_event_type.id})
           ) do
      case create_event do
        %{rejected: true} ->
          {:rejected, state}

        %{rejected: false} ->
          {:ok,
           %{
             state
             | :state_events =>
                 Map.put(state.state_events, PlasmaHS.EventTypes.m_room_create(), create_event)
           }}
      end
    else
      x -> x
    end
  end
end
