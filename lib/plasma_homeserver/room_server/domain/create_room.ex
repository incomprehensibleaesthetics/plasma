defmodule PlasmaHS.RoomServer.Domain.CreateRoomRequest do
  use Ecto.Schema
  alias PlasmaHS.RoomServer.Domain.CreateRoomRequest

  @matrix_config Application.get_env(:plasma, :matrix)
  @default_room_version @matrix_config[:default_room_version]
  @supported_room_versions @matrix_config[:supported_room_versions]

  @visibilities ["public", "private"]
  @presets ["private_chat", "public_chat", "trusted_private_chat"]

  embedded_schema do
    field :visibility, :string
    field :sender_id, :binary_id
    field :room_version, :string, default: @default_room_version
    field :creation_content, :map, default: %{}
    field :power_level_content_override, :map
    field :preset, :string
    field :name, :string
    field :topic, :string
    embeds_many :initial_state, StateEvent do
      field :type, :string
      field :state_key, :string, default: ""
      field :content, :string
    end
  end

  def validate(params \\ %{}) do
    %CreateRoomRequest{}
    |> Ecto.Changeset.cast(params, [
      :visibility,
      :room_version,
      :creation_content,
      :power_level_content_override,
      :name,
      :sender_id,
      :topic
    ])
    |> Ecto.Changeset.cast_embed(:initial_state, with: &cast_state_event/2)
    |> Ecto.Changeset.validate_required([:sender_id])
    |> Ecto.Changeset.validate_inclusion(:visibility, @visibilities, message: "bad_type")
    |> Ecto.Changeset.validate_inclusion(:preset, @presets, message: "bad_type")
    |> Ecto.Changeset.validate_inclusion(:room_version, @supported_room_versions, message: "unsupported_room_version")
    |> Ecto.Changeset.apply_action(:insert)
  end

  defp cast_state_event(schema, params) do
    schema
    |> Ecto.Changeset.cast(params, [:type, :state_key, :content])
    |> Ecto.Changeset.validate_required([:type, :state_key])
  end
end