defmodule PlasmaRepo.Repo.Migrations.ChangeColumnsInDevices do
  use Ecto.Migration

  def change do
    rename table("devices"), :initial_device_display_name, to: :display_name
  end
end
