defmodule Plasma.Repo.Migrations.CreateEventTable do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :mx_event_id, :string
      add :state_key, :string
      add :origin_server_ts, :utc_datetime
      add :received_ts, :utc_datetime
      add :sender_id, references(:users), null: false
      add :content, :map
      add :room_id, references(:rooms), null: false
      add :event_type_id, references(:event_types), null: false
      add :rejected, :boolean
      add :rejection_cause, :string
      timestamps()
    end
  end
end
