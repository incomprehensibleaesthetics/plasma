defmodule PlasmaRepo.Repo.Migrations.CreateEventEdges do
  use Ecto.Migration

  def change do
    create table(:event_edges, primary_key: false) do
      add :child_event_id, references(:events), null: false
      add :parent_event_id, references(:events), null: false
    end
  end
end
