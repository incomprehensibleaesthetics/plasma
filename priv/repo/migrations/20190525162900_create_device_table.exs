defmodule PlasmaRepo.Repo.Migrations.CreateDeviceTable do
  use Ecto.Migration

  def change do
    create table(:devices) do
      #add :id, :uuid, primary_key: true
      add :mx_device_id, :string, null: false
      add :initial_device_display_name, :string
      add :user_id, references(:users), null: false
      timestamps()
    end
  end
end
