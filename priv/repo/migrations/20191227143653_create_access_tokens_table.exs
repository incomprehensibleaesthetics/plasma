defmodule Plasma.Repo.Migrations.CreateAccessTokensTable do
  use Ecto.Migration

  def change do
    create table(:access_tokens) do
      #add :id, :uuid, primary_key: true
      add :user_id, references(:users, [on_delete: :delete_all])
      add :device_id, references(:devices, [on_delete: :delete_all])
      add :expires_at, :utc_datetime
      timestamps()
    end

    create unique_index(:access_tokens, [:device_id])
  end
end
