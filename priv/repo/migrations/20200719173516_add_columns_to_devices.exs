defmodule PlasmaRepo.Repo.Migrations.AddColumnsToDevices do
  use Ecto.Migration

  def change do
    alter table(:devices) do
      add :last_seen_ip, :text
      add :last_seen_ts, :utc_datetime
    end
  end
end
