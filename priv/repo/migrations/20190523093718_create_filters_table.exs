defmodule PlasmaRepo.Repo.Migrations.Help do
  use Ecto.Migration

  def change do
    create table(:filters) do
      add :filter_id, :string
      add :user_id, :string
      add :query, :map
      timestamps()
    end

    create unique_index(:filters, [:filter_id, :user_id])
  end
end
