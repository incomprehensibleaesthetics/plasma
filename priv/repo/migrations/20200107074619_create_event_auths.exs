defmodule PlasmaRepo.Repo.Migrations.CreateEventAuths do
  use Ecto.Migration

  def change do
    create table(:event_auths, primary_key: false) do
      add :event_id, references(:events), null: false
      add :auth_event_id, references(:events), null: false
    end
  end
end
